**# Code Refactoring using Bandit**

Various types of vulnerabilities exist in an application which can be extremely harmful if exploited. Different types of Injection Attacks such as SQL Injection, Command Injection, assert statements, polluted site-packages or import path, etc. These vulnerabilities can be caught in a python web application using Bandit. It is a tool designed to find common security issues in Python code. To do this Bandit processes
each file, builds an Abstract Syntax Tree (AST) from it, and runs appropriate plugins against the AST nodes. Once Bandit has finished scanning all the files it generates a report.

These scripts can be used to refactor the code so as to prevent threats to the said python application. Here, we explore various existing vulnerabilities in python ranging from security threat levels
of Low to High, describe the attack that can potentially harm the system, run bandit on these applications to detect the vulnerability and further propose code refactoring for mitigation of the vulnerabilities.

**Modules**
The vulnerabilities that are considered in this project are :
- Command Injection
- SQL Injection
- Cryptographic Vulnerabilities
- Vulnerabilities in python’s YAML library
- Vulnerability in Python’s Pickle Module
- flask debug true Vulnerability

There are scripts used for the refactoring of these above listed vulnerabilities.

For more information about the plugins of bandit see: https://bandit.readthedocs.io/en/latest/plugins/index.html
